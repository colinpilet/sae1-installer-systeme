﻿# SAE n°1 - Installer un système

## Objectif
Installer Xubuntu en Dual-Boot.
### Ce qu’il vous faut :

- Une clé USB vierge
- Un ordinateur
- le logiciel Rufus

## Les étapes
- Préparer la clé USB pour l’installation de Xubuntu
- Préparer de la place pour Xubuntu
- Installer Xubuntu sur votre ordinateur
## Préparer la clé USB pour l’installation de Xubuntu
Télécharger le logiciel Rufus sur le site suivant : [Rufus](https://rufus.ie/fr/)

Télécharger l’iso de xubuntu sur le lien suivant : [Iso Xubuntu](http://ftp.free.fr/mirrors/ftp.xubuntu.com/releases/22.04/release/xubuntu-22.04.1-desktop-amd64.iso)

Lancer Rufus et mettre les paramètres suivants :

- Dans dispositif sélectionner votre clé USB
- Dans type de démarrage choisir le fichier iso télécharger précédemment
- Dans schéma de partition choisir GPT
- Dans système de destination choisir BIOS ou UEFI

Laisser par défaut la section `Option de Formatage`.

**__ATTENTION__** cela va formater votre clé USB, veuillez à avoir sauvegarder vos fichier.

**__Félicitations vous venez de créer une clé contenant votre système__**

## Préparer de la place pour Xubuntu
Afin de pouvoir faire fonctionner Windows et Xubuntu en même temps sur le même ordinateur, il va falloir faire de la place sur le disque dur. Pour ce faire nous allons partitionner le disque dur de l'ordinateur. 
Sur Windows, il faut aller dans `gestion des disques` ( clique droit sur l'icone de windows puis gestion des disques ). Ensuite cliquer ( avec un clique droit ) sur le disque que vous voulez partitionner, et sélectionner `Réduire le volume`. Enfin choisir la taille que vous voulez allouer à votre partition Xubuntu ( au minimun de 25 Go ) et cliquer sur réduire.

**__Félicitations vous venez de préparer la place pour Xubuntu__**

## Installer Xubuntu sur votre ordinateur
### Configurer le BIOS
Brancher votre clé USB sur votre ordinateur et redémarrer celui-ci.

Pour pouvoir démarrer sur votre clé USB il va falloir modifier les paramètres de votre BIOS. Au démarrage appuyez sur la touche F2 de votre clavier ( ou les touches F10, F12 et suppr. Se référer à la documentation de votre ordinateur ).

Une fois dans le BIOS, dirigez-vous vers le menu BOOT ( à l’aide des flèches ou de votre souris ). Ensuite mettre la clé USB en haut de la liste `EFI`.

Redémarrer votre ordinateur.
### Début de l'installation
Votre ordinateur devrait redémarrer sur la clé USB et afficher une page avec plusieurs choix possibles. Sélectionner `Try or install Xubuntu`.
Après un chargement, la page de bienvenue s'affiche. Sur cette page sélectionnez votre langue puis cliquez sur `Installer Xubuntu`.
Ensuite, laisser par défaut les paramètres de disposition du clavier et cliquer sur continuer.
Si vous souhaitez connecter votre ordinaieur à internet vous pouvez vous connecter à votre réseau ( vous pourrez aussi le faire plus tard ).
Une fois sur la page `Mise à jour et autres logiciels` cocher `installation normale`, si ceci n'est pas déja coché. Cela permet d'avoir déja installé certains logiciels, par exemple Libre-Office . Cocher aussi `installer un logiciel tiers[...]médias supplémentaires` afin d'avoir les drivers nécessaires au support des cartes Wi-Fi et cartes graphiques. Vous pouvez aussi choisir de télécharger les mises à jours si vous avez connecté votre ordinateur à internet précédemment ( peut se faire plus tard ). Ensuite, choisir installer à côté de windows ( afin de pouvoir avoir un Dual-BOOT ). Dans l'onglet `Où êtes-vous` laisser tel quel. Enfin rentrer toutes les informations qui sont demandées ( bien choisir un mot de passe sécurisé et ne pas l'oublier, il permet d'effectuer toute les installations ). 
Après un temps de chargement plus ou moins long, Xubuntu est installé.

**__Félicitations vous venez d'installer Xubuntu__**

**__Bonne utilisation à vous__**

# Mes choix
Dans cette partie je vais expliquer mes choix pour l'installation.

Tout d'abord, j'ai fait le choix de faire une installation en Dual-BOOT car j'utilise encore beaucoup Windows et que Xubuntu n'est que secondaire.

Pour le logiciel de création de clé bootable, j'ai choisi Rufus pour sa simplicité et car c'est un logiciel très fiable et performant. 
J'ai aussi choisi le format GPT pour le formatage de la clé car c'est le format le plus polyvalent.

# Mon GitLab

Voici le lien vers mon [GitLab](https://gitlab.com/colinpilet/sae1-installer-systeme), afin de récupérer les différents fichiers évoqués dans ce README.